Bootstrap: docker
From: ubuntu:20.04

%labels
    Author IFB
    Version 2.0c

%files
    /src/tmhmm/2.0c/tmhmm-2.0c.Linux.tar.gz /opt/
    protein.fasta /opt/

%post
    # Install dependencies
    ## From apt
    apt-get -qq update && apt-get -qq upgrade -y
    apt-get -qq install -y wget gzip locales
    apt-get -qq clean && rm -rf /var/lib/apt/lists/*

    localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8


    ## From conda
    wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    chmod +x Miniconda3-latest-Linux-x86_64.sh
    ./Miniconda3-latest-Linux-x86_64.sh -b -s -p /opt/conda
    rm Miniconda3-latest-Linux-x86_64.sh
    export PATH=/opt/conda/bin:$PATH
    conda update -q -y conda
    conda install -q -c conda-forge -y mamba
    mamba install -q -y -c conda-forge -c bioconda -c defaults\
          conda-forge::perl \
          bioconda::perl-getopt-long \
          conda-forge::gnuplot

    cd /opt/
    tar -zvxf tmhmm-2.0c.Linux.tar.gz
    rm -rf tmhmm-2.0c.Linux.tar.gz

    cd tmhmm-2.0c/bin
    ln -s decodeanhmm.Linux_x86_64 decodeanhmm
    sed -i "s|/usr/local/bin/perl|/opt/conda/bin/perl|" tmhmm
    sed -i "s|/usr/local/bin/perl|/opt/conda/bin/perl|" tmhmmformat.pl

%environment
    export PATH=/opt/tmhmm-2.0c/bin:/opt/conda/bin:$PATH
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/x86_64-linux-gnu/:/opt/conda/lib
    export LANG=en_US.utf8

%runscript
    echo "DTU Health Tech TMHMM is dedicated to Academic usage and requires a license agreement: https://services.healthtech.dtu.dk/cgi-bin/sw_request"
    exec "$@"

%test
    export PATH=/opt/tmhmm-2.0c/bin:/opt/conda/bin:$PATH
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/x86_64-linux-gnu/:/opt/conda/lib
    export LANG=en_US.utf8

    cd /opt/
    tmhmm protein.fasta | grep "sp|P08100|OPSD_HUMAN Number of predicted TMHs:  7"
    decodeanhmm 2>&1 | grep "decodeanhmm 1.1g"
